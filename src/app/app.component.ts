import { AfterContentInit, AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { SpinnerService } from './core/services/spinner.service';
import { AuthService } from './core/services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  isLoaded!: boolean;
  routerSubscription!: Subscription;
  spinnerSusbscription!: Subscription;
  authSubscription!: Subscription;
  isAuth: boolean = false;
  constructor(
    private router: Router,
    private spinnerService: SpinnerService,
    private authService: AuthService

  ) {

  }
  ngOnInit(): void {
    this.isLoaded = false;
    this.routerSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.isLoaded = true;
      } else if (event instanceof NavigationEnd) {
        this.isLoaded = false;
      }
    }
    );
    this.spinnerSusbscription = this.spinnerService.returnObservable().subscribe(
      (data) => {
        setTimeout(() => {
          this.isLoaded = data;
        }, 0);
      }
    );
    this.authSubscription = this.authService.authObv().subscribe(
      (data) => {
        console.log(data);
        this.isAuth = data;
      }
    );
  }

  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
    this.spinnerSusbscription.unsubscribe();
    this.authSubscription.unsubscribe();
  }
}
