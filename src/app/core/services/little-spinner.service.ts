import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LittleSpinnerService {
  private _isLoading = new BehaviorSubject<any>({ value: false, message: '' });
  constructor() {
  }
  returnObservable() {
    return this._isLoading.asObservable();
  }
  showSpinner(message: any) {
    this._isLoading.next({ value: true, message: message });
  }
  hideSpinner() {
    this._isLoading.next({ value: false, message: '' });
  }
}
