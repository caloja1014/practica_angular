import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _urlVehiculos = '';
  constructor(private http: HttpClient) {
  }

  getPeliculas() {
    return this.http.get<any>(environment.API_URL);
  }
  postPelicula(vehiculo: any) {
    return this.http.post<any>(environment.API_URL, vehiculo);
  }
  updatePelicula(id: number, vehiculo: any) {
    return this.http.put<any>(environment.API_URL + '/' + id, vehiculo);
  }
  deletePelicula(id: number) {
    return this.http.delete<any>(environment.API_URL + '/' + id);
  }
}

