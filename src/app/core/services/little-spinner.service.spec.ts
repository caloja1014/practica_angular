import { TestBed } from '@angular/core/testing';

import { LittleSpinnerService } from './little-spinner.service';

describe('LittleSpinnerService', () => {
  let service: LittleSpinnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LittleSpinnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
