import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  private _isLoading = new BehaviorSubject<boolean>(false);
  constructor() {
  }
  returnObservable() {
    return this._isLoading.asObservable();
  }
  showSpinner() {
    this._isLoading.next(true);
  }
  hideSpinner() {
    this._isLoading.next(false);
  }
}
