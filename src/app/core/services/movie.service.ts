import { Injectable } from '@angular/core';
import { Pelicula } from '../interfaces/pelicula.interface';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  peliculas: Pelicula[];
  updating: boolean = false;
  constructor() {
    this.peliculas = [];
  }
  setPeliculas(peliculas: any) {
    this.peliculas = peliculas as Pelicula[];
  }
  getPeliculas() {
    return this.peliculas;
  }
  deleteMovie(id: number) {
    this.peliculas.forEach((element, index) => {
      if (element.id == id) this.peliculas.splice(index, 1);
    });
  }
  addMovie(pelicula: Pelicula) {
    this.peliculas.push(pelicula);
  }
  updateMovie(pelicula: Pelicula) {
    this.peliculas.forEach(peli => {
      if (peli.id === pelicula.id) {
        peli.name = pelicula.name;
        peli.sinopsis = pelicula.sinopsis;
        peli.year = pelicula.year;
      }
    });
  }
  isUpdating(): boolean {
    return this.updating;
  }
}
