import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private urlRegister = '/users/register';
  private urlLogin = '/users/login';
  private auth = new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient) {
  }

  register(user: any) {
    return this.http.post<any>(environment.API_URL_AUTH + this.urlRegister, user);
  }
  login(user: any) {
    return this.http.post<any>(environment.API_URL_AUTH + this.urlLogin, user);
  }
  signOut() {
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("refreshToken");
    this.auth.next(false);
    window.location.reload();
  }
  isAuthenticated(): boolean {
    const token = sessionStorage.getItem('token');
    return token ? true : false;
  }
  getAuth() {
    return this.auth;
  }
  authObv() {
    return this.auth.asObservable();
  }
  getToken(): any {
    return sessionStorage.getItem('token');
  }
  refresToken(): any {
    const refreshToken = sessionStorage.getItem('refreshToken');
    return this.http.post('/auth/refreshtoken', {
      refreshToken: refreshToken
    });
  }
  getRefreshToken(): any {
    return sessionStorage.getItem('refreshToken');
  }
}
