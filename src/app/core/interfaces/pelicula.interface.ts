/* eslint-disable camelcase */

export interface Pelicula {
  id: number;
  name: string;
  producer: string;
  avgScore: number;
  year: number;
  sinopsis: string;
  imageCover: string;
  imageBanner: string;
}
