export class ConverterSnakeCamel {
  private static toCamel(s: string): any {
    return s.replace(/([-_][a-z])/ig, ($1) => {
      return $1.toUpperCase()
        .replace('_', '');
    });
  }

  public static keysToCamel(o: any): any {
    if (o === Object(o) && !Array.isArray(o) && typeof o !== 'function') {
      const n: { [index: string]: any } = {};
      Object.keys(o)
        .forEach((k) => {
          n[ConverterSnakeCamel.toCamel(k)] = this.keysToCamel(o[k]);
        });
      return n;
    } else if (Array.isArray(o)) {
      return o.map((i) => {
        return this.keysToCamel(i);
      });
    }
    return o;
  }
  private static toSnake(str: string): any {
    return str.replace(/([A-Z])/g, '_$1').toLowerCase();
  }

  public static keysToSnake(o: any): any {
    if (o === Object(o) && !Array.isArray(o) && typeof o !== 'function') {
      const n: { [index: string]: any } = {};
      Object.keys(o)
        .forEach((k) => {
          n[ConverterSnakeCamel.toSnake(k)] = this.keysToSnake(o[k]);
        });
      return n;
    } else if (Array.isArray(o)) {
      return o.map((i) => {
        return this.keysToSnake(i);
      });
    }
    return o;
  }
}
