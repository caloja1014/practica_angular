import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
var users: any = JSON.parse(localStorage.getItem('users') || '[]');

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  constructor() {
    users.push({
      id: 1,
      username: 'admin',
      password: 'admin',
    });
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const url: any = request.url;
    const method: any = request.method;
    const body: any = request.body;
    const headers: any = request.headers;
    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());
    function handleRoute() {
      switch (true) {
        case url.endsWith('/users/login') && method === 'POST':
          return authenticate();
        case url.endsWith('/users/register') && method === 'POST':
          return register();
        default:
          return next.handle(request);
      }
    }


    function authenticate() {
      const username: any = body.username;
      const password: any = body.password;
      const user = users.find((x: { username: any; password: any; }) => x.username === username && x.password === password);
      if (!user) {
        return of(new HttpResponse({
          status: 409, body: {
            error: 'El usuario o contraseña no coinciden'
          }
        }));
      }
      const bodySend = {
        user: { ...user },
        token: 'FL4GSVQS4W5CKSFRVZBLPIVZZJ2K4VIFPLGZ45SJGUQK4SS56IWPWACZ7V2B7OVLVKZCNK5JZSSW7CIHCNQJAO3TOUE3375108HHTLY',
        refreshToken: 'SPFPQ5IBLB6DPE6FKPWHMIWW4MCRICX4M4KQXFQMI6THZXIEZ6QGNWNOERD6S7655LJAFWTRIKC4KGYO5G3XROMEOTBSS53CFSB6GIA',
      };
      bodySend.user.password = undefined;
      return of(new HttpResponse({ status: 200, body: bodySend }));
    }

    function register() {
      const user: any = body;

      if (users.find((x: { username: any; }) => x.username === user.username)) {
        return of(new HttpResponse({
          status: 409, body: {
            error: 'Username "' + user.username + '" ya ha sido tomado'
          }
        }));
      }

      user.id = users.length ? Math.max(...users.map((x: { id: any; }) => x.id)) + 1 : 1;
      user.profileUrl = 'https://i.pravatar.cc/150?img=35';
      user.address = 'Duran';
      user.birthday = '14/10/2000';
      user.identification = '0951690148';
      user.genero = 'Femenino';
      users.push(user);
      localStorage.setItem('users', JSON.stringify(users));
      return of(new HttpResponse({ status: 200, body: {} }));
    }
  }
}
