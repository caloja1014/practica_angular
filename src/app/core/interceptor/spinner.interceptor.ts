import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { SpinnerService } from '../services/spinner.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MovieService } from '../services/movie.service';
@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
  constructor(private spinnerService: SpinnerService, private movieService: MovieService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.movieService.getPeliculas().length === 0 || this.movieService.isUpdating()) {
      this.spinnerService.showSpinner();
      return next.handle(request).pipe(tap(
        event => {
          if (event instanceof HttpResponse) {
            setTimeout(() => {
              this.spinnerService.hideSpinner();
            }, 500);
          }
        },
        error => {
          if (error instanceof HttpErrorResponse) {
            (error);
            setTimeout(() => {
              this.spinnerService.hideSpinner();
            }, 500);
          }
        }
      ));
    }
    return next.handle(request);
  }
}

