import { ValidatorFn, AbstractControl } from '@angular/forms';

export class PasswordValidator {
  public static validator(): ValidatorFn {
    return (control: AbstractControl) => {
      const password = control.value;
      const regex = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/);
      const valid = regex.test(password);
      return valid ? null : { 'invalidPassword': true };
    };
  }
}
