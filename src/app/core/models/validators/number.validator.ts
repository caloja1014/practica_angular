import { ValidatorFn, AbstractControl } from '@angular/forms';

export class NumberValidator {
  public static validator(): ValidatorFn {
    return (control: AbstractControl) => {
      const url = control.value;
      const regex = new RegExp(/^[1-9]\d/);
      const valid = regex.test(url);
      return valid ? null : { 'invalidText': true };
    };
  }
}
