import { ValidatorFn, AbstractControl } from '@angular/forms';
export class YearValidator {
  public static validator(): ValidatorFn {
    return (control: AbstractControl) => {
      const year = control.value;
      const regex = new RegExp('^[0-9]*$');
      const valid = regex.test(year);
      return valid ? null : { 'invalidYear': true };
    };
  }
  public static validatorLength(): ValidatorFn {
    return (control: AbstractControl) => {
      const year = control.value;
      const regex = new RegExp(/^\d+$/);
      if (year!=null &&year.length > 4 && regex.test(year)) {
        return { 'invalidLengthYear': true };
      }
      return null;
    };
  }
}
