import { ValidatorFn, AbstractControl } from '@angular/forms';

export class UrlValidator {
  public static validator(): ValidatorFn {
    return (control: AbstractControl) => {
      const url = control.value;
      const regex = new RegExp(/^(((ftp|http|https):\/\/)|www)[^ "]+$/);
      const valid = regex.test(url);
      return valid ? null : { 'invalidUrl': true };
    };
  }
}
