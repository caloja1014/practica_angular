import { ValidatorFn, AbstractControl } from '@angular/forms';
export class EmailValidator {
  public static validator(): ValidatorFn {
    return (control: AbstractControl) => {
      const email = control.value;
      const regex = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      const valid = regex.test(email);
      return valid ? null : { 'email': true };
    };
  }
}
