import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mensaje: string = '';
  isSubmitted: boolean = false;
  formLogin !: FormGroup;
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }
  ngOnInit(): void {
    this.formLogin = this.buildFormLogin();
  }

  login() {
    this.isSubmitted = true;
    if (this.formLogin.invalid) {
      return;
    }
    this.authService.login(this.formLogin.value).toPromise().then(
      (res) => {
        this.formLogin.reset();
        if (res.error) this.mensaje = res.error;
        else {
          this.router.navigateByUrl('/home');
          this.authService.getAuth().next(true);
          localStorage.setItem('user', JSON.stringify(res.user));
          sessionStorage.setItem('token', res.token);
          sessionStorage.setItem('refreshToken', res.refreshToken);
        }
      }
    );
  }
  get formLoginControl() {
    return this.formLogin.controls;
  }
  private buildFormLogin(): FormGroup {
    return new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }
}
