import { Component, OnDestroy, OnInit } from '@angular/core';
import { Pelicula } from '../../../core/interfaces/pelicula.interface';
import { ApiService } from '../../../core/services/api.service';
import { DialogAddMovieComponent } from '../../components/dialog-add-movie/dialog-add-movie.component';
import { MatDialog } from '@angular/material/dialog';
import { ConverterSnakeCamel } from 'src/app/core/utils/converterSnakeCamel.util';
import { MovieService } from 'src/app/core/services/movie.service';
import { LittleSpinnerService } from 'src/app/core/services/little-spinner.service';
import { Subscription, merge, Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  title = 'practicaAngular1';
  peliculas: Pelicula[] = [];
  mostrarFrom: boolean = false;
  mostrarSubSpinner: boolean = false;
  isLoaded: boolean = false;
  message: String = "";
  litteSuscription!: Subscription;
  cardsLayout: Observable<any>;
  constructor(
    private apiService: ApiService,
    private dialog: MatDialog,
    private movieService: MovieService,
    private littleSpinnerService: LittleSpinnerService,
    private breakpointObserver: BreakpointObserver
  ) {
    this.cardsLayout = merge(this.breakpointObserver.observe([Breakpoints.Handset, Breakpoints.XSmall, Breakpoints.Small]).pipe(
      map(({ matches }) => {
        console.debug('tablet layout activated' + this.cardsLayout);
        if (matches) {
          return this.getHandsetLayout();
        }
        return this.getDefaultLayout();
      })),
      this.breakpointObserver.observe(Breakpoints.Tablet).pipe(
        map(({ matches }) => {
          if (matches) {
            return this.getTabletLayout();
          }
          return this.getDefaultLayout();
        })),
      this.breakpointObserver.observe(Breakpoints.Web).pipe(
        map(({ matches }) => {
          if (matches) {
            return this.getWebLayout();
          }
          return this.getDefaultLayout();
        })));
  }


  ngOnInit() {
    this.peliculas = this.movieService.getPeliculas();
    this.getAllPeliculas();
    this.litteSuscription = this.littleSpinnerService.returnObservable().subscribe(
      (res) => {
        console.log(res);
        this.isLoaded =res.value;
        this.message = res.message;
      }
    );
  }

  getAllPeliculas() {
    this.apiService.getPeliculas().toPromise().then(
      (res) => {
        const camelList = res.map((element: any) => {
          return ConverterSnakeCamel.keysToCamel(element);
        });
        (res);
        this.movieService.setPeliculas(camelList);
        this.peliculas = this.movieService.getPeliculas();
      }
    );
  }
  openDialog() {
    const dialogRef = this.dialog.open(DialogAddMovieComponent, { width: '50vw' });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  ngOnDestroy(): void {
    this.litteSuscription.unsubscribe();
  }
  getHandsetLayout(): any {
    return {
      name: 'Handset',
      gridColumns: 1,
      cols: 1,
      rows: 1
    };
  }

  getTabletLayout(): any {
    return {
      name: 'Tablet',
      gridColumns: 4,
      cols: 2,
      rows: 1
    };
  }

  getWebLayout(): any {
    return {
      name: 'Web',
      gridColumns: 6,
      cols: 2,
      rows: 1
    };
  }

  getDefaultLayout(): any {
    return {
      name: 'default',
      gridColumns: 1,
      cols: 1,
      rows: 1
    };
  }
}
