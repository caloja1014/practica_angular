import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogRegisterComponent } from '../../components/dialog-register/dialog-register.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EmailValidator } from 'src/app/core/models/validators/email.validator';
import { PasswordValidator } from 'src/app/core/models/validators/password.validator';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  formRegister!: FormGroup;
  mensaje: string = '';
  isSubmitted = false;
  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private router: Router
  ) { }
  ngOnInit(): void {
    this.formRegister = this.buildFomrRegister();
  }

  register() {
    this.isSubmitted = true;
    if (!this.formRegister.valid) return false;

    this.authService.register(this.formRegister.value).toPromise().then(
      (data) => {
        this.formRegister.reset();
        if (data.error) {
          this.mensaje = data.error;
        } else {
          this.dialog.open(
            DialogRegisterComponent,
            {}
          );
          this.router.navigate(['/home']);
        }
      }
    );
    return true;
  }
  get formRegisterControl() {
    return this.formRegister.controls;
  }
  private buildFomrRegister() {
    return new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, PasswordValidator.validator()]),
      email: new FormControl('', [Validators.required, EmailValidator.validator()]),
    });
  }
}
