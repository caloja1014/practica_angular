import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ApiService } from '../../../core/services/api.service';
import { MovieService } from 'src/app/core/services/movie.service';
import { YearValidator } from 'src/app/core/models/validators/year.validator';
import { UrlValidator } from 'src/app/core/models/validators/url.validator';
import { NumberValidator } from 'src/app/core/models/validators/number.validator';
import { ConverterSnakeCamel } from 'src/app/core/utils/converterSnakeCamel.util';
import { DialogMessageComponent } from '../dialog-message/dialog-message.component';
import { MatDialog } from '@angular/material/dialog';
import SwiperCore, { Navigation } from "swiper";
import { LittleSpinnerService } from 'src/app/core/services/little-spinner.service';
SwiperCore.use([Navigation]);
@Component({
  selector: 'app-dialog-add-movie',
  templateUrl: './dialog-add-movie.component.html',
  styleUrls: ['./dialog-add-movie.component.css']
})
export class DialogAddMovieComponent implements OnInit {
  formPelicula!: FormGroup;
  isSubmitted: boolean = false;
  constructor(
    private apiService: ApiService,
    private dialogRef: MatDialogRef<DialogAddMovieComponent>,
    private movieService: MovieService,
    private dialog: MatDialog,
    private littleSpinner: LittleSpinnerService
  ) { }
  ngOnInit(): void {
    this.formPelicula = this.buildFormPelicula();
  }

  createPelicula() {
    this.isSubmitted = true;
    if (!this.formPelicula.valid) return false;
    const snakePeli = ConverterSnakeCamel.keysToSnake(this.formPelicula.value);
    this.littleSpinner.showSpinner("Creando...");
    this.apiService.postPelicula(snakePeli).toPromise().then(
      async (res) => {
        this.movieService.addMovie(this.formPelicula.value);
        this.formPelicula.reset();
        this.littleSpinner.hideSpinner();
        this.dialog.open(
          DialogMessageComponent,
          {
            data: {
              title: 'Exito!',
              message: 'Se ha agregado con éxito la pelicula'
            }
          }
        );
      }
    );
    this.closeDialog();
    return true;
  }
  closeDialog() {
    this.dialogRef.close();
  }

  get formPeliculaControl() {
    return this.formPelicula.controls;
  }
  private buildFormPelicula(): FormGroup {
    return new FormGroup({
      name: new FormControl('', Validators.required),
      producer: new FormControl('', Validators.required),
      avgScore: new FormControl('', [Validators.required, NumberValidator.validator()]),
      year: new FormControl('', [Validators.required, YearValidator.validatorLength(), YearValidator.validator()]),
      sinopsis: new FormControl('', Validators.required),
      imageCover: new FormControl('', [Validators.required, UrlValidator.validator()]),
      imageBanner: new FormControl('', [Validators.required, UrlValidator.validator()]),
    });
  }
}
