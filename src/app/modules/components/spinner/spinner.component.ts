import { Input, SimpleChange } from '@angular/core';
import { Component } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent {
  @Input() loading: boolean = false;
  ngOnChanges(changes: SimpleChange) {
  }
}
