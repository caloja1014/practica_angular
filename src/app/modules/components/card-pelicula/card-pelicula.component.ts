import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MovieService } from 'src/app/core/services/movie.service';
import { YearValidator } from '../../../core/models/validators/year.validator';
import { ConverterSnakeCamel } from 'src/app/core/utils/converterSnakeCamel.util';
import { DialogMessageComponent } from '../dialog-message/dialog-message.component';
import { MatDialog } from '@angular/material/dialog';
import { LittleSpinnerService } from 'src/app/core/services/little-spinner.service';

@Component({
  selector: 'app-card-pelicula',
  templateUrl: './card-pelicula.component.html',
  styleUrls: ['./card-pelicula.component.css']
})
export class CardPeliculaComponent implements OnInit {
  @Input() pelicula: any;
  editar: boolean = false;
  formPelicula !: FormGroup;
  isSubmitted: boolean = false;
  constructor(
    private apiService: ApiService,
    private movieService: MovieService,
    private dialog: MatDialog,
    private littleSpinnerService: LittleSpinnerService
  ) { }

  ngOnInit(): void {
    this.formPelicula = this.buildFormPelicula();
    this.formPelicula.controls.name.setValue(this.pelicula.name);
    this.formPelicula.controls.year.setValue(this.pelicula.year);
    this.formPelicula.controls.sinopsis.setValue(this.pelicula.sinopsis);
    (this.pelicula);
  }
  eliminarPelicula(id: number) {
    this.littleSpinnerService.showSpinner("Eliminando...");
    this.apiService.deletePelicula(id).toPromise().then(
      (res) => {
        this.movieService.deleteMovie(id);
        this.littleSpinnerService.hideSpinner();
        this.dialog.open(
          DialogMessageComponent,
          {
            data: {
              title: 'Éxito',
              message: 'Se ha eliminado con éxito la pelicula'
            }
          }
        );
      }
    );
  }
  changeEditar() {
    this.editar = !this.editar;
  }
  updatePelicula() {
    this.isSubmitted = true;
    if (this.formPelicula.invalid) {
      return;
    }
    const snakecase = ConverterSnakeCamel.keysToSnake(this.formPelicula.value);
    this.movieService.updating=true;
    this.apiService.updatePelicula(this.pelicula.id, snakecase).toPromise().then(
      async (res) => {
        this.editar = false;
        this.movieService.updateMovie({ id: this.pelicula.id, ...this.formPelicula.value });
        this.dialog.open(
          DialogMessageComponent,
          {
            data: {
              title: 'Éxito',
              message: 'Se ha actualizado con éxito la pelicula'
            }
          }
        );
        this.movieService.updating=false;
      }
    );
  }
  get formPeliculaControl() {
    return this.formPelicula.controls;
  }
  private buildFormPelicula(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required, YearValidator.validatorLength(), YearValidator.validator()]),
      sinopsis: new FormControl('', [Validators.required]),
    });
  }
}
