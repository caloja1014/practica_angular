import { Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-little-spinner',
  templateUrl: './little-spinner.component.html',
  styleUrls: ['./little-spinner.component.css']
})
export class LittleSpinnerComponent {
  @Input() loading: boolean = false;
  @Input() message: String = "";
  constructor() { }
}
