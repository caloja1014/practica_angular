import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-custom-menu-user',
  templateUrl: './custom-menu-user.component.html',
  styleUrls: ['./custom-menu-user.component.css']
})
export class CustomMenuUserComponent {
  constructor(private router: Router) { }
  goToHome() {
    this.router.navigateByUrl('/home');
  }
  closeSesion() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('refreshToken');
    window.location.reload();
  }
  goToProfile() {
    this.router.navigateByUrl('/profile');
  }
}
