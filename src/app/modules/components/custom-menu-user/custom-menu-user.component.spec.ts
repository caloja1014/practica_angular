import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomMenuUserComponent } from './custom-menu-user.component';

describe('CustomMenuUserComponent', () => {
  let component: CustomMenuUserComponent;
  let fixture: ComponentFixture<CustomMenuUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomMenuUserComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomMenuUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
