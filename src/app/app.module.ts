import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardPeliculaComponent } from './modules/components/card-pelicula/card-pelicula.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TokenInterceptorService } from './core/interceptor/token-interceptor.service';
import { LoginComponent } from './modules/pages/login/login.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { HomeComponent } from './modules/pages/home/home.component';
import { RegisterComponent } from './modules/pages/register/register.component';
import { FakeBackendInterceptor } from './core/interceptor/fake-backend.interceptor';
import { DialogAddMovieComponent } from './modules/components/dialog-add-movie/dialog-add-movie.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogRegisterComponent } from './modules/components/dialog-register/dialog-register.component';
import { SpinnerComponent } from './modules/components/spinner/spinner.component';
import { SpinnerInterceptor } from './core/interceptor/spinner.interceptor';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CustomTopBarComponent } from './modules/components/custom-top-bar/custom-top-bar.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { CustomMenuUserComponent } from './modules/components/custom-menu-user/custom-menu-user.component';
import { ProfileComponent } from './modules/pages/profile/profile.component';
import { MatCarouselModule } from '@ngbmodule/material-carousel';
import { DialogMessageComponent } from './modules/components/dialog-message/dialog-message.component';
import { SwiperModule } from 'swiper/angular';
import { LittleSpinnerComponent } from './modules/components/little-spinner/little-spinner.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppComponent,
    CardPeliculaComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    DialogAddMovieComponent,
    DialogRegisterComponent,
    SpinnerComponent,
    CustomTopBarComponent,
    CustomMenuUserComponent,
    ProfileComponent,
    DialogMessageComponent,
    LittleSpinnerComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatCarouselModule.forRoot(),
    SwiperModule,
    FlexLayoutModule,
    LayoutModule,
    MatGridListModule

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: FakeBackendInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
